﻿namespace RotF_Administrative_Tool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customerBox = new System.Windows.Forms.GroupBox();
            this.confirmPassText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.passText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.emailText = new System.Windows.Forms.TextBox();
            this.secondaryEmailText = new System.Windows.Forms.TextBox();
            this.friendlyNameText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.addBtn = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.playerIDCmb = new System.Windows.Forms.ComboBox();
            this.customerBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // customerBox
            // 
            this.customerBox.Controls.Add(this.playerIDCmb);
            this.customerBox.Controls.Add(this.label7);
            this.customerBox.Controls.Add(this.addBtn);
            this.customerBox.Controls.Add(this.label6);
            this.customerBox.Controls.Add(this.label5);
            this.customerBox.Controls.Add(this.label4);
            this.customerBox.Controls.Add(this.friendlyNameText);
            this.customerBox.Controls.Add(this.secondaryEmailText);
            this.customerBox.Controls.Add(this.confirmPassText);
            this.customerBox.Controls.Add(this.label3);
            this.customerBox.Controls.Add(this.passText);
            this.customerBox.Controls.Add(this.label2);
            this.customerBox.Controls.Add(this.label1);
            this.customerBox.Controls.Add(this.emailText);
            this.customerBox.Location = new System.Drawing.Point(12, 12);
            this.customerBox.Name = "customerBox";
            this.customerBox.Size = new System.Drawing.Size(349, 366);
            this.customerBox.TabIndex = 0;
            this.customerBox.TabStop = false;
            this.customerBox.Text = "New Website Customer";
            // 
            // confirmPassText
            // 
            this.confirmPassText.Location = new System.Drawing.Point(141, 104);
            this.confirmPassText.Name = "confirmPassText";
            this.confirmPassText.Size = new System.Drawing.Size(190, 20);
            this.confirmPassText.TabIndex = 5;
            this.confirmPassText.UseSystemPasswordChar = true;
            this.confirmPassText.TextChanged += new System.EventHandler(this.confirmPassText_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "*User Password (confirm)";
            // 
            // passText
            // 
            this.passText.Location = new System.Drawing.Point(141, 66);
            this.passText.Name = "passText";
            this.passText.Size = new System.Drawing.Size(190, 20);
            this.passText.TabIndex = 3;
            this.passText.UseSystemPasswordChar = true;
            this.passText.TextChanged += new System.EventHandler(this.passText_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "*User Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "*User Email";
            // 
            // emailText
            // 
            this.emailText.Location = new System.Drawing.Point(141, 28);
            this.emailText.Name = "emailText";
            this.emailText.Size = new System.Drawing.Size(190, 20);
            this.emailText.TabIndex = 0;
            this.emailText.TextChanged += new System.EventHandler(this.emailText_TextChanged);
            // 
            // secondaryEmailText
            // 
            this.secondaryEmailText.Location = new System.Drawing.Point(141, 145);
            this.secondaryEmailText.Name = "secondaryEmailText";
            this.secondaryEmailText.Size = new System.Drawing.Size(190, 20);
            this.secondaryEmailText.TabIndex = 6;
            this.secondaryEmailText.TextChanged += new System.EventHandler(this.secondaryEmailText_TextChanged);
            // 
            // friendlyNameText
            // 
            this.friendlyNameText.Location = new System.Drawing.Point(141, 187);
            this.friendlyNameText.Name = "friendlyNameText";
            this.friendlyNameText.Size = new System.Drawing.Size(190, 20);
            this.friendlyNameText.TabIndex = 7;
            this.friendlyNameText.TextChanged += new System.EventHandler(this.friendlyNameText_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "User Secondary Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 190);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "User Friendly Name";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 277);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "*Required";
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(141, 282);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(189, 28);
            this.addBtn.TabIndex = 11;
            this.addBtn.Text = "Add / Update";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 234);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Player ID";
            // 
            // playerIDCmb
            // 
            this.playerIDCmb.FormattingEnabled = true;
            this.playerIDCmb.Location = new System.Drawing.Point(141, 234);
            this.playerIDCmb.Name = "playerIDCmb";
            this.playerIDCmb.Size = new System.Drawing.Size(121, 21);
            this.playerIDCmb.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 661);
            this.Controls.Add(this.customerBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.customerBox.ResumeLayout(false);
            this.customerBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox customerBox;
        private System.Windows.Forms.TextBox passText;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox emailText;
        private System.Windows.Forms.TextBox confirmPassText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox friendlyNameText;
        private System.Windows.Forms.TextBox secondaryEmailText;
        private System.Windows.Forms.ComboBox playerIDCmb;
        private System.Windows.Forms.Label label7;
    }
}

