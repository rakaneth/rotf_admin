﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace RotF_Administrative_Tool
{
    public partial class Form1 : Form
    {
        private bool ValidateUpdate
        {
            get
            {
                bool hasEmail = emailText.Text != null;
                bool hasPass = !(passText.Text == null || passText.Text == "");
                bool hasConfirm = !(confirmPassText.Text == null || confirmPassText.Text == "");
                bool passMatch = passText.Text == confirmPassText.Text;
                return hasEmail && hasPass && hasConfirm && passMatch;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            string confirmText = $"Email: {emailText.Text}\nSecondary Email: {secondaryEmailText.Text}\nFriendly Name: {friendlyNameText.Text}\n";
            confirmText += $"Player ID: {playerIDCmb.SelectedValue}";
            var confirmUpdate = MessageBox.Show(confirmText, "Confirm Customer Update", MessageBoxButtons.YesNo);
            if (confirmUpdate == DialogResult.Yes)
            {
                string connString = System.Configuration.ConfigurationManager.ConnectionStrings["webCustTest"].ConnectionString;
                using (var conn = new System.Data.SqlClient.SqlConnection(connString))
                {
                    conn.Open();
                    string sql = "EXEC sp_CreateCustomer @pass, @email, @email2, @friend, @playerid";
                    var cmd = new System.Data.SqlClient.SqlCommand(sql);
                    cmd.Parameters.Add("@pass", SqlDbType.NVarChar).Value = passText.Text;
                    cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = emailText.Text;
                    if (secondaryEmailText.Text != null && secondaryEmailText.Text != "")
                    {
                        cmd.Parameters.Add("@email2", SqlDbType.NVarChar).Value = secondaryEmailText.Text;
                    }
                    else
                    {
                        cmd.Parameters.Add("@email2", SqlDbType.NVarChar).Value = DBNull.Value;
                    }
                    if (friendlyNameText.Text == null || friendlyNameText.Text == "")
                    {
                        cmd.Parameters.Add("@friend", SqlDbType.NVarChar).Value = DBNull.Value;
                    }
                    else
                    {
                        cmd.Parameters.Add("@friend", SqlDbType.NVarChar).Value = friendlyNameText.Text;
                    }
                    cmd.Parameters.Add("@playerid", SqlDbType.Int).Value = (int)playerIDCmb.SelectedValue;
                    int result = cmd.ExecuteNonQuery();
                }
            }      
        }

        private void emailText_TextChanged(object sender, EventArgs e)
        {
            addBtn.Enabled = ValidateUpdate;
        }

        private void passText_TextChanged(object sender, EventArgs e)
        {
            addBtn.Enabled = ValidateUpdate;
        }

        private void confirmPassText_TextChanged(object sender, EventArgs e)
        {
            addBtn.Enabled = ValidateUpdate;
        }

        private void secondaryEmailText_TextChanged(object sender, EventArgs e)
        {
            addBtn.Enabled = ValidateUpdate;
        }

        private void friendlyNameText_TextChanged(object sender, EventArgs e)
        {
            addBtn.Enabled = ValidateUpdate;
        }
    }
}
